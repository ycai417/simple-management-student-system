import java.util.*;

 /**
 * A class to store and get a student's information.
 * 
 * @author (YANGYANG CAI) 
 * @version (09/05/2014)
 */
public class Student
{
    //Create a String value to store student's name.
    //Create a String value to store student's telephone.
    //Create a StudentSubject value to store student's subjects.
    private String studentName;
    private String studentTelephone;
    private StudentSubject studentSubjects;
    
    /**
     * Constructor for Student objects.  
     */
    public Student()
    {
        studentName = "No name";
        studentTelephone = "No telephone";
        studentSubjects = new StudentSubject();
    }

    /**
     * Constructor for Student objects. 
     * @param String value of student's name.
     * @param String value of student's telephone.
     * @param String value of student's subjects.
     */
    public Student(String name, 
                   String telephone, 
                   String subjectsList)
    {
        setName(name);
        setTelephone(telephone);
        setSubjects(subjectsList);
    }

    /**
     * This method checks the student's subjects.
     * @return boolean value of whether the student has a given subject.
     */
    public boolean hasSubject(String subject)
    {
        return studentSubjects.hasSubject(subject);
    }

    /**
     * This method gets student's name.
     * @return String value of student's name.
     */
    public String getName()
    {

        return studentName;
    }
    
    /**
     * This method gets student's telephone.
     * @return String value of student's telephone.
     */
    public String getTelephone()
    {
        return studentTelephone;
    }   

    /**
     * This method gets student's subjects.
     * @return String value of student's subjects.
     */
    public String getSubjects()
    {
        if(studentSubjects.getSubjectsCount()!=0)
            return studentSubjects.getSubjects();
        else
            return "No sujbect";
    }

    /**
     * This method sets student's name.
     * @param String value of student's name.
     */
    public void setName(String newName)
    {   
        if (newName.trim().equals("")){
            studentName = "No name";
            return;
        }
            
        String[] nameParts = newName.split(" +");    
        studentName = "";
        // Captialize the first letter of student's name.
        for (String namePart : nameParts)
        {
            namePart = namePart.substring(0, 1).toUpperCase() + namePart.substring(1).toLowerCase();
            studentName += namePart + " ";
        }
        
        studentName = studentName.trim();
    }

    /**
     * This method sets student's telephone.
     * @param String value of student's telephone.
     */
    public void setTelephone(String newTelephone) 
    {
        if (!newTelephone.trim().equals(""))
            studentTelephone = newTelephone;
        else
            studentTelephone = "No telephone";
    }

    /**
     * This method sets student's subjects.
     * @param String value of student's subjects.
     */
    public void setSubjects(String newSubjects)
    {
        studentSubjects = new StudentSubject();
        
        if (!newSubjects.trim().equals(""))
            studentSubjects.setSubjects(newSubjects);
    }
   
    /**
     * This method gets student's information.
     * @return String value of the student's information for displaying in the Student System.
     */
    public String stringStudent(){
        return ( "Name:\t\t" + getName() + 
                "\nTelephone:\t" + getTelephone() +
                 "\nSubject:\t" + getSubjects() + "\n");
    }
    
    /**
     * This method gets student's information.
     * @return String value of the student's information for saving in the Student Database.
     */
    @Override
    public String toString()
    {
        return getName() + "," + getTelephone() + "," + getSubjects();
    }
}
