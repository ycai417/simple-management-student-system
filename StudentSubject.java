import java.util.*;

/**
 * A class to store and get student's subjects information.
 * 
 * @author (YANGYANG CAI) 
 * @version (20/05/2014)
 */
public class StudentSubject
{
    //Create a ArrayList<String> value to store student's subjects.
    private ArrayList<String> studentSubjects;
    
    /**
     * Constructor for StudentSubject objects. 
     */
    public StudentSubject()
    {
        studentSubjects = new ArrayList<String>();
    }

    /**
     * Constructor for StudentSubject objects. 
     * @param String value of student's subjects.
     */
    public StudentSubject(String subjectList)
    {
        studentSubjects = new ArrayList<String>();
        String[] subjects = subjectList.split("/");

        for (String subject : subjects )
            studentSubjects.add(subject.trim().toLowerCase());
    }
   
    /**
     * This method checks the student's subjects.
     * @return boolean value of whether the student has a given subject.
     */
    public boolean hasSubject(String subject)
    {
        for(String studentSubject: studentSubjects) {
            if(studentSubject.equalsIgnoreCase(subject.trim()))
                return true;
        }
       return false;
    }

    /**
     * This method gets student's subjects.
     * @return String value of student's subjects.
     */
    public String getSubjects()
    {
        String subjects = "";
        
        for (String subject : studentSubjects)
        {            
            subjects += subject + " ";
        }
        
        //Clear the last space and format the string of subjects.
        return subjects.trim().replace(" ", "/");
    }

    /**
     * This method sets student's subjects.
     * @param String value of student's subjects.
     */
    public void setSubjects(String subjectList)
    {
        studentSubjects = new ArrayList<String>();
        String[] subjects = subjectList.split("/+");
            
        for (String subject : subjects)
        {
            subject = subject.trim();
            //Capitalize the first letter of student's subjects.
            subject = subject.substring(0,1).toUpperCase() + subject.substring(1).toLowerCase();
            studentSubjects.add(subject);
        }
    }

    /**
     * This method return the number of student's subjects.
     * @param String value of student's subjects.
     */
    public int getSubjectsCount()
    {
        return studentSubjects.size();     
    }
}
