import java.util.Scanner;

/**
 * A system class to manage student information.
 * 
 * @author (YANGYANG CAI) 
 * @version (11/05/2014)
 */
public class StudentSystem
{
    //Create a StudentDabase value to store students' information.
    //Create a boolean value to control the Student System's running.
    private StudentsDatabase students;
    private boolean isRunning;

    /**
     * Constructor for StudentSystem objects. 
     */
    public StudentSystem()
    {
        students = new StudentsDatabase();
        students.readFile("students.txt");
        isRunning = true;
    }

    /**
     * This main method for the class.
     */
    public static void main(String[] args)
    {
        StudentSystem system = new StudentSystem();
        system.displaySystem();
    }

    /**
     * This method adds a new student to the Student System.
     */
    private void addNewStudent()
    {
        String[] studentString = new String[3];
        studentString[0] = inputName();
        studentString[1] = inputTelephone();
        studentString[2] = inputSubjects();

        Student student = new Student(studentString[0], studentString[1], studentString[2]);
        students.addStudent(student);
        System.out.println("\nThe student information: " + student.toString() + " was added.\n");
    }

    /**
     * This method check the student's name. 
     * The name is non-blank and must be unique.
     * @return boolean value of whether the name is vaild.
     */
    private boolean checkName(String newName) 
    {
         if (newName.trim().equals("")) 
         {
            System.out.println("Student Name can not be blank!\n");  
            return false;   
         }
         else
            if (students.checkNameUnique(newName))
            {
                System.out.println("Student name: " + newName + " already exists.\n" );
                return false;
            }
         else
            return true;
    }

    /**
     * This method checks the student's telephone. 
     * The telephone is non-blank.
     * The telephone must have 9 digists and 1st digit must be 9.
     * @return boolean value of whether the telephone is vaild.
     */
    private boolean checkTelephone(String newTelephone) 
    {
       if (!newTelephone.trim().matches("^9\\d{8}$"))
          {
              System.out.println("Telephone number must have 9 digits and 1st digit must be 9!\n");
              return false;
          }
       else      
          return true;
    }

    /**
     * This method checks the student's every subject. 
     * The subject is non-blank.
     * Only three subjects are offered: Computing, Maths, English.
     * @return boolean value of whether the subject is vaild.
     */
    private boolean checkSingleSubject(String newSubject) 
    {
        if (newSubject.trim().equals(""))
        {
            System.out.println("Student subject can not be blank!\n");
            return false;
        }
        else 
        {
            if (newSubject.trim().equalsIgnoreCase("computing") ||
                newSubject.trim().equalsIgnoreCase("maths") || 
                newSubject.trim().equalsIgnoreCase("english") )
                return true;
            else
            {
                System.out.println("Only 3 subjects are offered : Computing, Maths and English.\n");
                return false;
            }
        }
    }
   
    /**
     * This method check the student's subject subjects.
     * Every subject must be unique.
     * @return boolean value of whether the subject is unique.
     */
    public boolean checkSubjectUnique(String subjectsList)
    {
        String[] subjects = subjectsList.trim().split("/+");
        boolean result = true;

        for (int i = 0; i < subjects.length - 1; i ++)
        {
            for (int j = i + 1 ; j < subjects.length; j ++)
                if (subjects[i].equals(subjects[j]))
                {
                    result = false;
                    System.out.println("The student's every subject must be unique.\n");
                }
        }

        return result;        
    }
    
     /**
     * This method checks the student's all subjects. 
     * Every subject is non-blank.
     * Every subject must be offered.
     * Every subject must be unique.
     * @return boolean value of whether all the subjects are vaild.
     */
    private boolean checkAllSubjects(String subjectsList) 
    {     
        String[] subjects = subjectsList.trim().split("/+", -1);
        boolean subjectsAllVaild = true;
        
        for (String subject : subjects)
        {
            if (!checkSingleSubject(subject))
                subjectsAllVaild = false;
        }

        if (!checkSubjectUnique(subjectsList))
            subjectsAllVaild = false;

        return subjectsAllVaild;
    }

    /**
     * This method deletes a student from the Student System.
     */
    private void deleteStudent()
    {
        System.out.print("Input the student name: ");
        Scanner console = new Scanner(System.in);
        String name = console.nextLine();
        boolean isFinished = false;

        if (name.trim().equals(""))
        {
            System.out.println("The student name can not be blank.\n" +
                                "You do not delete any student.\n");
            return;
        }
        else
        { 
            if (students.selectByName(name) == null)
                System.out.println("The student " + name + " does not exist.\n");          
            else
            {
                while (!isFinished)
                {
                    System.out.println("Are you sure you want to detele the student " + name + " ? ");
                    System.out.print("Y/N: ");
                    String option  = console.nextLine().trim().toUpperCase();

                    switch (option)  
                    {
                        case "Y": System.out.println("The student " + name + " was deleted.\n");
                                  students.deleteByName(name);
                                  isFinished = true; break;
                        case "N": System.out.println("You do not delete any student.\n");
                                  isFinished = true; break;
                        default: System.out.println("Please choose the option from Y/N.\n");
                    }
                }
            }  
        }       
    }
    
    /**
     * This method displays the Student System.
     */
    public void displaySystem()
    {
        while (isRunning)
        {
            showMenu();

            switch (getOption().trim())
            {
                case "1": System.out.println("Option 1 is selected.\n");
                          addNewStudent(); break;
                case "2": System.out.println("Option 2 is selected.\n");
                          deleteStudent(); break;
                case "3": System.out.println("Option 3 is selected.\n");
                          findStudent(); break;
                case "4": System.out.println("Option 4 is selected.\n");
                          listStudents(); break;
                case "5": System.out.println("Option 5 is selected.\n");
                          listAllStudents(); break;
                case "6": System.out.println("Option 6 is selected.\n");
                          exitSystem(); break;
                default : System.out.println("Please choose the option from 1 to 6.\n");
            }
        }
    }

    /**
     * This method exits the Student System.
     */
    private void exitSystem()
    {
         boolean isFinished = false;

         while (!isFinished)
         {
            System.out.println("Are you sure you want to exist the system ? ");
            System.out.print("Y/N: ");
            Scanner console = new Scanner(System.in);
            String option  = console.nextLine().trim().toUpperCase();

            switch (option)  
            {
                case "Y": System.out.println("Welcome to the Simply Student Management System.");
                          System.out.println("The " + students.numberOfStudents() + " students " +
                                             "has saved to the 'students.txt'.\n");
                          students.writeFile("students.txt");
                          isFinished = true;
                          isRunning = false; break;
                case "N": System.out.println("");
                          isFinished = true; break;
                default: System.out.println("Please choose the option from Y/N.\n");
            }
         }
    }

    /**
     * This method finds and lists a student by name in the Student System.
     */
    private void findStudent()
    {     
        System.out.print("Input the student name: ");
        Scanner console = new Scanner(System.in);
        String name = console.nextLine();
 
        if (students.selectByName(name) != null)
            System.out.println(students.selectByName(name).stringStudent());
        else
            System.out.println("The student " + name + " does not exist.\n");
    }

     /**
     * This method gets your option.
     * @return String value of your option.
     */
    private String getOption()
    {
        System.out.print("Choose your option: ");
        Scanner console = new Scanner(System.in);
        String option = console.nextLine();
        return option;
    }
    
    /**
     * This method get the value of input name.
     * @return Strng value of student's name.
     */
    private String inputName()
    {
        boolean isFinished = false;
        String newName = "";
        
        while (!isFinished)
        {
            System.out.print("Input student name: ");
            Scanner console = new Scanner(System.in);
            String name = console.nextLine();

            String[] nameParts = name.trim().split(" ");
            newName = "";
            for(String namePart:nameParts){
                newName += namePart + " ";
            }            
            newName = newName.trim();
            
            if (checkName(newName)) 
            {
                isFinished = true;  
            }             
        }

        return newName;
    }

    /**
     * This method get the value of input telephone.
     * @return Strng value of student's telephone.
     */
    private String inputTelephone()
    {
        boolean isFinished = false;
        String newTelephone = "";

        while (!isFinished)
        {
            System.out.print("Input student telephone: ");
            Scanner console = new Scanner(System.in);
            String telephone = console.nextLine();

            if (checkTelephone(telephone)) 
            {
                isFinished = true;
                newTelephone = telephone.trim();
            }
        }

        return newTelephone;
    }
    
    /**
     * This method get the value of input subjects.
     * @return Strng value of student's subjects.
     */
    private String inputSubjects()
    {   
        boolean isFinished = false;
        String newSubjects = "";

        while (!isFinished)
        {
            System.out.print("Input student subjects with '/' (e.g.maths/computing)\nSubjects: ");
            Scanner console = new Scanner(System.in);
            String subjectsList = console.nextLine(); 

            if (checkAllSubjects(subjectsList))
            {
                isFinished = true;   
                newSubjects = subjectsList.trim();
            }
        }

        return newSubjects;
    }

    /**
     * This method lists the student by subject in the Student System.
     */   
    private void listStudents()
    {                
        System.out.print("Input student subjects with '/' (e.g.maths/computing)\nSubjects: ");
        Scanner console = new Scanner(System.in);
        String subjectsList = console.nextLine();
        int count = 0;

        if (!checkAllSubjects(subjectsList))
            return;       
        else
        {
            if (students.selectBySubjects(subjectsList).size() != 0)
            {
                for (Student student : students.selectBySubjects(subjectsList))
                {
                    count ++;
                    System.out.println("Student #" + count + "\n" + student.stringStudent());
                }
            }
            else 
                System.out.println("The student has the subjects " + 
                                   subjectsList.trim() + " is not exist.\n");
        }
    }
  
    /**
     * This method lists all the students in the Student System.
     */
    private void listAllStudents()
    {
         int count = 0;

         for (Student student : students.selectAll())
         {
              count ++;
              System.out.println("Student #" + count + "\n" + student.stringStudent());
         }
     }
    
    /**
     * This method shows the menu of the Student System.
     */
    private void showMenu()
    {
        System.out.println("Welcome to the Simply Student Management System");
        System.out.println("===============================================");
        System.out.println("(1) Add new student");
        System.out.println("(2) Delete a student");
        System.out.println("(3) Find Student By Name");
        System.out.println("(4) List Students By Subject");
        System.out.println("(5) List All Students");
        System.out.println("(6) Exit System");
    }  
}   