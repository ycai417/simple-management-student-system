import java.io.File;
import java.io.*;
import java.util.*;

/**
 * A database class to store the students' information.
 * 
 * @author (YANGYANG CAI) 
 * @version (06/05/2014)
 */
public class StudentsDatabase 
{
    // create a ArrayList<Student> value to store students' information.
    private ArrayList<Student> students;

    /**
     * Constructor for StudentDatabase objects. 
     */
    public StudentsDatabase()
    {
        students = new ArrayList<Student>();
    }

    /**
     * This method adds a student to the Student Database.
     * @param Student value of a new student.
     */
    public void addStudent(Student newStudent)
    {
          students.add(newStudent);
    }

    /**
     * This method check the student's name. 
     * The name must be unique.
     * @return boolean value of whether the name is unique.
     */
    public boolean checkNameUnique(String newName)
    {
        return (selectByName(newName) != null);           
    }


    /**
     * This method deleteds a student from the Student Database.
     * @param String value of the student's name.
     */
    public void deleteByName(String name)
    {
       students.remove(selectByName(name));
    }

    /**
     * This method calcutes the number of the students in the Student Database.
     * @return int value of the numbers.
     */
    public int numberOfStudents()
    {
        return students.size();
    }

    /**
     * This method read a file to get all the data of the Students Database.
     *  @param String value of file's name. 
     */
    public void readFile(String fileName)
    {      
        BufferedReader reader = null;

        try 
        {  
            reader = new BufferedReader(new FileReader(new File(fileName)));
            String newLine = reader.readLine();

            while (newLine != null)
            {
                String[] data = newLine.split(",");
                students.add(new Student(data[0], data[1], data[2]));
                newLine = reader.readLine();
            }
        }
        catch (FileNotFoundException e)
        {
            System.out.println("The file can not be found.");
        }
        catch (IOException e)
        {
            System.out.println("Unexpexted I/O exception occurs");
        }
        finally
        {
            try 
            {
                reader.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }   
    }   

    /**
     * This method select a student by name from the Student Database.
     * @param String value of the student's name.
     * @return Student value of the student.
     */
    public Student selectByName(String name)
    {
        for (Student student : students)
        {
            if (student.getName().equalsIgnoreCase(name.trim()))
                return student;
        } 

        return null;
    }

    /**
     * This method select students by subjects from the Student Database.
     * @param String value of the student's subjects.
     * @return ArrayList<Student> value of the students.
     */
    public ArrayList<Student> selectBySubjects(String subjectsList)
    {
        ArrayList<Student> result = new ArrayList<Student>();
        String[] subjects = subjectsList.trim().split("/+");  
    
        for (Student student : students)
        {
            boolean subjectsAllHad = true;   
                    
            for (String subject : subjects)
            {   
                if (!student.hasSubject(subject)){
                     subjectsAllHad = false;
                     break;
                }
            }     
            
            if (subjectsAllHad)
                result.add(student);       
        }  

        return result;          
    }  

    /**
     * This method selects all the students from the Student Database.
     * @return ArrayList<Student> value of all the students.
     */
    public ArrayList<Student> selectAll()
    {
        return students;
    }

    /**
     * This method write a file to save all the data of the Students Database.
     * @param String value of file's name. 
     */
    public void writeFile(String fileName)
    {
        BufferedWriter writer = null;
        
        try
        {
            writer = new BufferedWriter(new FileWriter(new File(fileName)));

            for (Student student : students)
            {
                writer.write(student.toString());
                writer.newLine();   
            } 
        }
        catch (IOException e)
        {   
             System.out.println("Unexpexted I/O exception occurs");
        }       
        finally 
        {
            try
            {
                writer.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
