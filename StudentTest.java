import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class StudentTest.
 *
 * @author  (YANGYANG CAI)
 * @version (10/05/2014)
 */
public class StudentTest
{
    private Student student1;
    private Student student2;

    /**
     * Default constructor for test class StudentTest
     */
    public StudentTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
        student1 = new Student();
        student2 = new Student("David Smith","912345678","Maths");
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }

    @Test
    public void testGetName()
    {
        assertEquals("No name", student1.getName());
        assertEquals("David Smith",student2.getName());
    }

    @Test
    public void testGetTelephone()
    {
        assertEquals("No telephone", student1.getTelephone());
        assertEquals("912345678",student2.getTelephone());
    }
    
    @Test
    public void testGetSubjects()
    {
        assertEquals("", student1.getSubjects());
        assertEquals("Maths",student2.getSubjects());
    }

    @Test
    public void testSetName()
    {
        student1.setName("Andy Cheng");
        assertEquals("Andy Cheng",student1.getName());
    }

    @Test
    public void testSetTelephone()
    {
        student1.setTelephone("987654321");
        assertEquals("987654321",student1.getTelephone());
    }
   
    @Test
    public void testSetSubjects()
    {
        student1.setSubjects("Computing/maths");
        assertEquals("Computing/maths",student1.getSubjects());
    }

    @Test
    public void testToString()
    {
        assertEquals("David Smith,912345678,Maths",student2.toString());
    }
}


